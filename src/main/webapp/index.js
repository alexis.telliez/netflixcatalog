var requestOptions = {
    method: 'GET',
    redirect: 'follow'
};

fetch("http://localhost:8080/NetflixCatalog/NetflixServlet", requestOptions)
    .then(response => response.text())
    .then(result => app.generateJson(result))
    .catch(error => console.log('error', error));

var app = new Vue({
    el: '#app',
    data: {
        lists: [ ]
    },
    methods: {
        generateJson: function(result){
            parsedResult = JSON.parse(result);
            parsedResult.forEach(result => {
                this.lists.push({name: result.name, nbSeasons: result.nbSeasons, nbEpisodes: result.nbEpisodes});
            });
        }
    }
})