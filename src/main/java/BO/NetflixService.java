package BO;

import DAO.SeasonDAO;
import DAO.SerieDAO;
import DO.SeasonDO;
import DO.SerieDO;
import DTO.CountSeasonDTO;

import java.util.ArrayList;
import java.util.List;

public class NetflixService {

    private SeasonDAO seasonDAO = new SeasonDAO();
    private SerieDAO SerieDAO = new SerieDAO();

    public List<CountSeasonDTO> findAll() {
        final List<SerieDO> series = SerieDAO.findAll();
        final List<SeasonDO> seasons = seasonDAO.findAll();
        final List<CountSeasonDTO> countSeasonDTO = new ArrayList<>();
        for (SerieDO serie : series) {
            CountSeasonDTO s = new CountSeasonDTO();
            s.setName(serie.getName());
            int nbSeasons = 0;
            int nbEpisodes = 0;
            for (SeasonDO season : seasons) {
                if (season.getFkSerie() == serie.getId()) {
                    nbSeasons++;
                    nbEpisodes += season.getNbEpisodes();
                }
            }
            s.setNbSeasons(nbSeasons);
            s.setNbEpisodes(nbEpisodes);
            countSeasonDTO.add(s);
        }
        return countSeasonDTO;
    }
}
