package DO;

public class SeasonDO {

    private int id;
    private int fkSerie;
    private int numSaison;
    private int nbEpisodes;

    public SeasonDO(int id, int fkSerie, int numSaison, int nbEpisodes) {
        this.id = id;
        this.fkSerie = fkSerie;
        this.numSaison = numSaison;
        this.nbEpisodes = nbEpisodes;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getFkSerie() {
        return fkSerie;
    }

    public void setFkSerie(int fkSerie) {
        this.fkSerie = fkSerie;
    }

    public int getNumSaison() {
        return numSaison;
    }

    public void setNumSaison(int numSaison) {
        this.numSaison = numSaison;
    }

    public int getNbEpisodes() {
        return nbEpisodes;
    }

    public void setNbEpisodes(int nbEpisodes) {
        this.nbEpisodes = nbEpisodes;
    }
}
