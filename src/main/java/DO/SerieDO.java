package DO;

public class SerieDO {
    private int id;
    private String name;
    private Boolean productionOriginal;
    private int year;

    public SerieDO(int id, String name, Boolean productionOriginal, int year) {
        this.id = id;
        this.name = name;
        this.productionOriginal = productionOriginal;
        this.year = year;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getProductionOriginal() {
        return productionOriginal;
    }

    public void setProductionOriginal(Boolean productionOriginal) {
        this.productionOriginal = productionOriginal;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }
}
