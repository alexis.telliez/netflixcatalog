package DTO;

import java.util.List;

public class HomeDTO {
    private List<CountSeasonDTO> listSeries;

    public List<CountSeasonDTO> getListSeries() {
        return listSeries;
    }

    public void setListSeries(List<CountSeasonDTO> listSeries) {
        this.listSeries = listSeries;
    }



}
