package DTO;

public class CountSeasonDTO {
    private String name;
    private int nbSeasons;
    private int nbEpisodes;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNbSeasons() {
        return nbSeasons;
    }

    public void setNbSeasons(int nbSeasons) {
        this.nbSeasons = nbSeasons;
    }

    public int getNbEpisodes() {
        return nbEpisodes;
    }

    public void setNbEpisodes(int nbEpisodes) {
        this.nbEpisodes = nbEpisodes;
    }
}
