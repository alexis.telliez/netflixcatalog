import BO.NetflixService;
import DTO.HomeDTO;
import com.google.gson.Gson;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

@WebServlet(name = "NetflixServlet", value = "/NetflixServlet")
public class NetflixServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    private NetflixService bo = new NetflixService();

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HomeDTO dto = new HomeDTO();
        dto.setListSeries(bo.findAll());

        response.setContentType("application/json");
        response.getWriter().append(new Gson().toJson(dto.getListSeries()));
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }


}